package modele

import "net"

/*

Lexique Role
0 = villageois
1 = loup garou
2 = chasseur

*/

type Pair struct {
	Predecesseur net.Conn
	Successeur   net.Conn
	Ip           int64
	Role         int
	Leader       bool
	Pseudo       string
}
