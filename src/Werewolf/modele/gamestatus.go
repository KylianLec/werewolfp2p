package modele

/**
 * Represente le statut d'une partie
 */
type GameStatus struct {
	Started bool
	Isnight bool
	Role    int
	State   string
	Dead    bool
}

func InitStatus() GameStatus {
	var gs GameStatus
	gs.Started = false
	gs.Dead = false
	gs.Isnight = false
	gs.Role = -1

	return gs
}
