package controlleurs

/*
 *
 * Format d'une requette :
 * id:::COMMAND:::<command>:::[<args> : ]    Pour envoyer une commande
 * id:::CHAT : <pseudo> : <message>    Pour envoyer un message sur le chat
 *
 */

import (
	"Werewolf/modele"
	"bufio"
	"bytes"
	"fmt"
	"math"
	"math/rand"
	"net"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
)

var gs modele.GameStatus
var pair modele.Pair
var pendingRequest []string
var processedRequest []string
var myElectionNum int64
var playerInGame []string
var wg sync.WaitGroup

var playersAliveRole map[string]int //Représente le nombre de joueur en vie trié par role

var nbVote int
var votes []string
var voteOk bool

/**
 * Fonction qui crée le serveur de connexion
 */
func CreateServer(becomeLeader bool) {
	fmt.Println("Creation du serveur de connexion ...")
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Port du serveur de connexion local (ex \"1234\")")
	fmt.Print(">> ")
	scanner.Scan()
	fmt.Println("ecoute sur :" + scanner.Text())

	// Creation du serveur
	listener, err := net.Listen("tcp", ":"+scanner.Text())

	// Si il y a une erreur (souvent port déjà occupé, on remet le menu)
	if err != nil {
		fmt.Println("Erreur lors de la création du serveur de connexion")
		MenuManager()
	}

	// On demande le pseudo et on le sauvegarde
	fmt.Println("Quel est ton pseudo ?")
	fmt.Print(">> ")
	scanner.Scan()
	pair.Pseudo = scanner.Text()

	for PseudoCorrect(pair.Pseudo) == false {
		fmt.Println("Votre pseudo contient des caractère incorrect : \" \" ou \":::\"")
		fmt.Println("Quel est ton pseudo ?")
		fmt.Print(">> ")
		scanner.Scan()
		pair.Pseudo = scanner.Text()
	}

	// Si l'utilisateur est le créateur du réseau, il devient leader par defaut
	if becomeLeader == true {
		pair.Leader = true
	} else { //Tous les autres joueurs reçoivent ce message
		fmt.Println("Patientez le temps que le créateur lance la partie !")
	}

	// On recupere l'adresse mac
	mac := strings.Join(strings.Split(getMacAddr(), ":"), "")

	// On convertis en int64
	myInt64Mac, err := strconv.ParseInt(mac, 16, 64)

	// On convertie le port en int64
	b := int64(listener.Addr().(*net.TCPAddr).Port)

	// On aditionne les deux
	pair.Ip = myInt64Mac + b

	gs.State = "WAITSTART"

	w := make(chan string)

	// On lance les routines
	go communication(w)
	go NetworkListenerForRead(w)
	go NetworkListenerForWrite(w)

	// Pour chaque nouvelle connexion, on lance une routine qui gere son inclusion dans le réseau
	for {
		conn, _ := listener.Accept()
		go NetworkListenerForNewConnection(conn)
	}
}

/**
 * Fonction pour rejoindre le réseau
 */
func JoinServer() {

	var (
		bonneEntree = false // variable qui permet de vérifier si l'utilisateur a rentré la valeur qu'on attend de lui
		scanner     = bufio.NewScanner(os.Stdin)
		validIP     = regexp.MustCompile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[1-9][0-9]*)$`)
	)

	fmt.Println("IP d'un client du jeu (ex 192.156.158.10:7789) :")
	fmt.Print(">> ")
	bonneEntree = false
	ipDuReseau := ""

	// Tant qu'on a pas une bonne entrée, on boucle
	for bonneEntree == false {
		scanner.Scan()
		ipDuReseau = scanner.Text()
		if !validIP.MatchString(ipDuReseau) {
			fmt.Println("Cette IP n'est pas valide, recommencez !")
			fmt.Print(">> ")
		} else {
			bonneEntree = true
		}
	}

	// On se connectre à l'ip entrée
	conn, err := net.Dial("tcp", ipDuReseau)
	if err != nil {
		fmt.Println("Error" + err.Error())
		MenuManager()
	}

	// On demande à entrer dans le réseau
	conn.Write([]byte(modele.RandString(10) + ":::COMMAND:::ENTER\n"))
	request, _ := bufio.NewReader(conn).ReadString('\n')

	// On decoupe la réponse
	processedRequest := ProcessRequest(request)

	// Si il nous donne l'ip de notre successeur
	if processedRequest[1] == "COMMAND" && processedRequest[2] == "IPSUC" {
		ipSuccesseur := processedRequest[3]

		// On demande le port de connexion de ce successeur
		fmt.Println("Quel est le port du serveur de connexion distant (nouveau successeur, ex \"1234\") ?")
		fmt.Print(">> ")
		scanner.Scan()
		ipSuccesseur = ipSuccesseur + ":" + scanner.Text()
		if pair.Predecesseur != nil {
			pair.Predecesseur.Close()
		}
		pair.Predecesseur = conn

		// On l'appelle
		conn2, err := net.Dial("tcp", ipSuccesseur)

		if err != nil {
			MenuManager()
		}

		// On demande de se connecter à lui
		conn2.Write([]byte(modele.RandString(10) + ":::COMMAND:::CONNECT\n"))

		if pair.Successeur != nil {
			pair.Successeur.Close()
		}
		pair.Successeur = conn2

		// On créee un serveur sur le quel on est pas leader par default
		CreateServer(false)

	} else {
		fmt.Println("Erreur lors de la reception")
	}
}

/**
 * Gere la lecture sur le réseau, les traites et renvois les réponses
 * Pour plus d'informations sur les requetes et leurs utilité, ce referer au compte rendu
 */
func NetworkListenerForRead(w chan string) {

	for {
		// Si c'est la fin du jeu, on sort de la routine
		if pair.Predecesseur != nil {
			requete, err := bufio.NewReader(pair.Predecesseur).ReadString('\n')
			if gs.State == "ENDOFGAME" {
				os.Exit(0)
			} else if err != nil {
				fmt.Println("Erreur : " + err.Error())

			} else {
				// On transforme la réponse
				processedRequest := ProcessRequest(requete)
				// Si on recois une commande
				if processedRequest[1] == "COMMAND" {

					// On regarde si elle est dans le tableau des commandes en attente de retour
					rep, i := Contains(pendingRequest, processedRequest[0])

					// Si oui, c'est qu'on est a l'origine de la requete, donc on traite la suite
					if rep {
						// On l'enleve du tableau
						pendingRequest[i] = pendingRequest[len(pendingRequest)-1]
						pendingRequest[len(pendingRequest)-1] = ""
						pendingRequest = pendingRequest[:len(pendingRequest)-1]

						// Switch qui traite differement pour chaque type de requetes
						if processedRequest[2] == "ELECTION" {
							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							pair.Leader = false
							w <- id + ":::COMMAND:::ARP:::" + processedRequest[3]
						} else if processedRequest[2] == "ARP" {

							myElectionNum = pair.Ip

							predeElectionNum, _ := strconv.ParseInt(processedRequest[3], 10, 64)

							if myElectionNum == predeElectionNum { //le Leader reste le leader
								pair.Leader = true

								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::ELECTIONOK"
							} else {
								pair.Leader = false
							}

						} else if processedRequest[2] == "ELECTIONOK" {
							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::START:::1"
						} else if processedRequest[2] == "START" {
							//Converti le nombre de joueur string en float
							nbrPlayerPartieFlt, _ := strconv.ParseFloat(processedRequest[3], 32)

							var nbWolfInt int 
							var nbVillInt int 
							var nbHunterInt int 

							//Il ne peut y avoir des chasseurs seulement si il y a au moins 4 joueurs
							if nbrPlayerPartieFlt == 2 || nbrPlayerPartieFlt == 3 {
								nbWolfInt = 1
								nbVillInt = 1
								nbHunterInt = 0
							} else {
								nbWolfInt = int(math.Ceil(0.3 * nbrPlayerPartieFlt))
								nbHunterInt = int(math.Ceil(0.1 * nbrPlayerPartieFlt))
								nbVillInt = int(nbrPlayerPartieFlt) - nbWolfInt - nbHunterInt
							}

							//On stock dans une map le nombre de joueur vivant par role
							playersAliveRole = make(map[string]int)
							playersAliveRole["wolf"] = nbWolfInt
							playersAliveRole["villagers"] = nbVillInt 
							playersAliveRole["hunter"] = nbHunterInt 

							myRole := rand.Intn(2)

							if myRole == 0 {
								fmt.Println("Les rôles ont été distribué, vous etes un villageois !")
								nbVillInt--
							} else if myRole == 1{
								fmt.Println("Les rôles ont été distribué, vous etes un loup-garou !")
								nbWolfInt--
							} else if myRole == 2 {
								fmt.Println("Les rôles ont été distribué, vous etes un chasseur !")
								nbHunterInt--
							}

							pair.Role = myRole

							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::ROLES:::" + strconv.Itoa(nbVillInt) + "," + strconv.Itoa(nbWolfInt) + "," + strconv.Itoa(nbHunterInt)
							playerInGame = append(playerInGame, pair.Pseudo)

						} else if processedRequest[2] == "ROLES" {

							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::PSEUDO:::" + pair.Pseudo

						} else if processedRequest[2] == "PSEUDO" {

							listOfPseudos := processedRequest[3:]
							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							playerInGame = processedRequest[3:]

							w <- id + ":::COMMAND:::PSEUDOS:::" + strings.Join(listOfPseudos, ":::")

						} else if processedRequest[2] == "PSEUDOS" {

							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::NIGHT"

						} else if processedRequest[2] == "NIGHT" {

							fmt.Println("C'est la nuit, le village s'endort")
							gs.State = "VOTEWOLF"

							// C'est au tour des loups garou de jouer
							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::LGVOTE"

						} else if processedRequest[2] == "LGVOTE" {
							nbVote = 0
							votes = votes[len(votes):]
							gs.State = "VOTEWOLF"
							if pair.Role == 1 && !gs.Dead{
								fmt.Println("Voici la liste des joueurs veuillez voter pour l'un d'eux !")
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}
						} else if processedRequest[2] == "LGVOTEOK" {
							votes = append(votes, processedRequest[3])
							//On attend d'avoir tous les votes
							if nbVote == playersAliveRole["wolf"] && pair.Leader == true {
								playerVoted := getVotedPlayer(votes)
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::DEATHROLE:::" + playerVoted + ":::WOLFKILL"
							} else if nbVote != playersAliveRole["wolf"] {
								fmt.Println("On attend que les autres loups-garous votent")
							}
						} else if processedRequest[2] == "HUNTERDIE" {
							if pair.Pseudo == processedRequest[3] {
								fmt.Println("Vous tiré une dernière balle avant de mourir, sur qui voulez vous tirer ?")
							} else {
								fmt.Println("Le chasseur tire une dernière balle avec de succomber à ses blessures...")
							}

							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::HUNTERVOTE:::" + processedRequest[3]

						} else if processedRequest[2] == "HUNTERVOTE"{
							gs.State = "HUNTERVOTE"
							if pair.Pseudo == processedRequest[3] {
								fmt.Println("Voici la liste des joueurs veuillez voter pour eliminer l'un d'eux !")
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}
						} else if processedRequest[2] == "HUNTERVOTEOK" {
							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							//processedRequest[3] = qui est mort
							w <- id + ":::COMMAND:::DEATHROLE:::" + processedRequest[3] + ":::HUNTERKILL"
						} else if processedRequest[2] == "DAY" {
							fmt.Println("C'est le jour, le village se réveil")
							gs.State = "VOTEVILL"

							id := modele.RandString(10)
							pendingRequest = append(pendingRequest, id)
							w <- id + ":::COMMAND:::VILLVOTE"
						
						} else if processedRequest[2] == "VILLVOTE" {
							nbVote = 0
							votes = votes[len(votes):]
							gs.State = "VOTEVILL"
							fmt.Println("Il est l'heure de voté pour éliminer quelqu'un ! ")
							if !gs.Dead {
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}
							
						} else if processedRequest[2] == "VILLVOTEOK" {
							fmt.Println(processedRequest[4] + " a voté pour : " + processedRequest[3])
							votes = append(votes, processedRequest[3])

							//On attend d'avoir tous les votes
							if nbVote == len(playerInGame) && pair.Leader == true {
								fmt.Println("tout le monde a voté, résultat...")
								playerVoted := getVotedPlayer(votes)

								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::DEATHROLE:::" + playerVoted + ":::VILLKILL"
							} else if nbVote != len(playerInGame){
								fmt.Println("On attend le vote des autres joueurs...")
							}

						} else if processedRequest[2] == "DEATHROLE" {//Changer kill par myrole(deathrole)
							if pair.Pseudo == processedRequest[3]{
								gs.Dead = true
								processedRequest = append(processedRequest, strconv.Itoa(pair.Role))

								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								//processedRequest[3] = qui est mort, processedRequest[4] = le tueur, processedRequest[5] = role du mort
								w <- id + ":::COMMAND:::SYNCKILL:::" + processedRequest[3] + ":::" + processedRequest[4] + ":::" + processedRequest[5]
							} else {
								w <- strings.Join(processedRequest, ":::")
							}

						} else if processedRequest[2] == "SYNCKILL" {
							//Un fois qu'on est revenu au leader, on passe à la commande myrole et on envoie
							if pair.Leader == true {
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::KILL:::" + processedRequest[3] + ":::" + processedRequest[4] + ":::" + processedRequest[5]
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
							
						} else if processedRequest[2] == "KILL" {
							if pair.Pseudo == processedRequest[3] { //c'est nous
								fmt.Println("Vous avez été tué par "+processedRequest[4]+", vous étiez un " + processedRequest[5])
							} else {
								fmt.Println(processedRequest[3] +" a été tué par "+processedRequest[4]+", c'était un " + processedRequest[5])
							}

							contain, i := Contains(playerInGame, processedRequest[3])
							if contain {
								playerInGame[i] = playerInGame[len(playerInGame)-1]
								playerInGame[len(playerInGame)-1] = ""
								playerInGame = playerInGame[:len(playerInGame)-1]
							}

							if pair.Leader {
								if processedRequest[5] == "0" {
									playersAliveRole["villagers"]--
									if playersAliveRole["villagers"] <= 0 && playersAliveRole["hunter"] <= 0{
										fmt.Println("Tous les villageois sont morts, les loups-garous ont gagné !")
										gs.State = "WIN"

										id := modele.RandString(10)
										pendingRequest = append(pendingRequest, id)
										w <- id + ":::COMMAND:::WOLFWIN"
									}
								} else if processedRequest[5] == "1" {
									playersAliveRole["wolf"]--
									if playersAliveRole["wolf"] <= 0 {
										fmt.Println("Tous les loups-garous sont morts, les villageois ont gagné !")
										gs.State = "WIN"

										id := modele.RandString(10)
										pendingRequest = append(pendingRequest, id)
										w <- id + ":::COMMAND:::VILLWIN"
									}
								} else if processedRequest[5] == "2" {
									playersAliveRole["hunter"]--
									if playersAliveRole["hunter"] <= 0 && playersAliveRole["villagers"] <= 0{
										fmt.Println("Tous les villageois sont morts, les loups-garous ont gagné !")
										gs.State = "WIN"

										id := modele.RandString(10)
										pendingRequest = append(pendingRequest, id)
										w <- id + ":::COMMAND:::WOLFWIN"
									} else {
										id := modele.RandString(10)
										pendingRequest = append(pendingRequest, id)
										w <- id + ":::COMMAND:::HUNTERDIE:::" + processedRequest[3] 
									}
								}
								//Si ce n'était pas un chasseur et qu'il n'y a pas de gagnant le jour se leve
								if processedRequest[5] != "2" && gs.State != "VOTEVILL" && gs.State != "WIN" {
									id := modele.RandString(10)
									pendingRequest = append(pendingRequest, id)
									w <- id + ":::COMMAND:::DAY"
								} else if gs.State == "VOTEVILL" {
									id := modele.RandString(10)
									pendingRequest = append(pendingRequest, id)
									w <- id + ":::COMMAND:::NIGHT"
								}
							}

						} else if processedRequest[2] == "VILLWIN" || processedRequest[2] == "WOLFWIN" {
							if processedRequest[2] == "VILLWIN" {
								fmt.Println("Félicitation aux villageois, ils ont gagnés")
							} else if processedRequest[2] == "WOLFWIN" {
								fmt.Println("Félicitation aux loups-garous, ils ont gagnés !")
							}
							gs.State = "ENDOFGAME"
							os.Exit(0)
						}
					} else { // Si on n'est pas à l'origine de la commande reçu
						// On traite la commande et on l'a renvois a notre successeur
						if processedRequest[2] == "ELECTION" && pair.Leader == false { //Si ce n'est pas le leader et qu'on reçoit election

							myElectionNum = pair.Ip
							predeElectionNum, _ := strconv.ParseInt(processedRequest[3], 10, 64) //string to int64

							//Test pour savoir quelle adresse mac est la plus grande, et donc celle qu'on va envoyer
							if myElectionNum > predeElectionNum {
								processedRequest[3] = strconv.FormatInt(myElectionNum, 10)
							} else if myElectionNum == predeElectionNum {
								// ici en cas d'égalité, non traité pour l'instant
							}

							//Envoie de la requête au successeur
							if pair.Successeur != nil {
								w <- strings.Join(processedRequest, ":::")
							}

						} else if processedRequest[2] == "ARP" {

							myElectionNum = pair.Ip

							predeElectionNum, _ := strconv.ParseInt(processedRequest[3], 10, 64)

							if myElectionNum == predeElectionNum { //le Leader reste le leader
								pair.Leader = true
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								// On dit que l'election c'est bien passé
								w <- id + ":::COMMAND:::ELECTIONOK"
							} else {
								pair.Leader = false
							}

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "ELECTIONOK" && pair.Leader == false {
							if pair.Successeur != nil {
								w <- strings.Join(processedRequest, ":::")
							}
						} else if processedRequest[2] == "START" {

							count, _ := strconv.Atoi(processedRequest[3])
							count++
							processedRequest[3] = strconv.Itoa(count)
							w <- strings.Join(processedRequest, ":::")

						} else if processedRequest[2] == "ROLES" {

							myRole := rand.Intn(2)
							myNewRequest := strings.Split(processedRequest[3], ",")

							for myNewRequest[myRole] == "0" {
								if myRole == 2 {
									myRole = 0
								} else {
									myRole++
								}
							}

							if myRole == 0 {
								fmt.Println("Les rôles ont été distribué, vous etes un villageois !")
							} else if myRole == 1{
								fmt.Println("Les rôles ont été distribué, vous etes un loup-garou !")
							} else if myRole == 2 {
								fmt.Println("Les rôles ont été distribué, vous etes un chasseur !")
							}

							pair.Role = myRole
							myNewRequestInt, _ := strconv.Atoi(myNewRequest[myRole])
							myNewNumber := myNewRequestInt - 1
							myNewRequest[myRole] = strconv.Itoa(myNewNumber)
							processedRequest[3] = strings.Join(myNewRequest, ",")
							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "PSEUDO" {

							w <- strings.Join(processedRequest, ":::") + ":::" + pair.Pseudo

						} else if processedRequest[2] == "PSEUDOS" {

							playerInGame = processedRequest[3:]

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "NIGHT" {
							fmt.Println("C'est la nuit, le village s'endort")
							gs.State = "VOTEWOLF"
							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "LGVOTE" {
							nbVote = 0
							votes = votes[len(votes):]
							if pair.Role == 1 && !gs.Dead {
								fmt.Println("Voici la liste des joueurs veuillez voter pour eliminer l'un d'eux !")
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}

							w <- strings.Join(processedRequest, ":::")

						} else if processedRequest[2] == "LGVOTEOK" {
							nbVote++
							if pair.Role == 1 {
								fmt.Println(processedRequest[4] + " a voté pour : " + processedRequest[3])
								votes = append(votes, processedRequest[3])
							}

							if nbVote == playersAliveRole["wolf"] && pair.Leader == true {
								playerVoted := getVotedPlayer(votes)
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::DEATHROLE:::" + playerVoted + ":::WOLFKILL"
							} else if nbVote != playersAliveRole["wolf"] {
								fmt.Println("On attend que les autres loups-garous votent")
								w <- strings.Join(processedRequest, ":::")
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
						
						} else if processedRequest[2] == "HUNTERDIE" {
							if pair.Pseudo == processedRequest[3] {
								fmt.Println("Vous tiré une dernière balle avant de mourir, sur qui voulez vous tirer ?")
							} else {
								fmt.Println("Le chasseur tire une dernière balle avec de succomber à ses blessures...")
							}

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "HUNTERVOTE" {
							gs.State = "HUNTERVOTE"
							if pair.Pseudo == processedRequest[3] { //On est le chasseur
								fmt.Println("Voici la liste des joueurs veuillez voter pour eliminer l'un d'eux !")
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "HUNTERVOTEOK" {
							if pair.Leader == true {
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								//processedRequest[3] = qui est mort
								w <- id + ":::COMMAND:::DEATHROLE:::" + processedRequest[3] + ":::HUNTERKILL"
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
							
						} else if processedRequest[2] == "DAY" {
							fmt.Println("C'est le jour, le village se réveil")
							gs.State = "VOTEVILL"
							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "VILLVOTE" {
							nbVote = 0
							votes = votes[len(votes):]
							
							if !gs.Dead {
								fmt.Println("Il est l'heure de voter ! ")
								fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
							}

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "VILLVOTEOK" {
							nbVote++
							fmt.Println(processedRequest[4] + " a voté pour : " + processedRequest[3])
							votes = append(votes, processedRequest[3])

							if nbVote == len(playerInGame) && pair.Leader == true {
								fmt.Println("tout le monde a voté, résultat...")
								playerVoted := getVotedPlayer(votes)

								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::DEATHROLE:::" + playerVoted + ":::VILLKILL"
							} else if nbVote != len(playerInGame) {
								fmt.Println("On attend le vote des autres joueurs...")
								w <- strings.Join(processedRequest, ":::")
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
						} else if processedRequest[2] == "DEATHROLE" { //Changer kill par myrole(deathrole)

							if pair.Pseudo == processedRequest[3] { //Si c'est nous
								gs.Dead = true
								processedRequest = append(processedRequest, strconv.Itoa(pair.Role))

								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								//processedRequest[3] = qui est mort, processedRequest[4] = le tueur, processedRequest[5] = role du mort
								w <- id + ":::COMMAND:::SYNCKILL:::" + processedRequest[3] + ":::" + processedRequest[4] + ":::" + processedRequest[5]
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
							
						} else if processedRequest[2] == "SYNCKILL" {
							//Un fois qu'on est revenu au leader, on passe à la commande myrole et on envoie
							if pair.Leader == true {
								id := modele.RandString(10)
								pendingRequest = append(pendingRequest, id)
								w <- id + ":::COMMAND:::KILL:::" + processedRequest[3] + ":::" + processedRequest[4] + ":::" + processedRequest[5]
							} else {
								w <- strings.Join(processedRequest, ":::")
							}
							
						} else if processedRequest[2] == "KILL" {
							if pair.Pseudo == processedRequest[3] { //c'est nous
								fmt.Println("Vous avez été tué par "+processedRequest[4]+", vous étiez un " + processedRequest[5])
							} else {
								fmt.Println(processedRequest[3] +" a été tué par "+processedRequest[4]+", c'était un " + processedRequest[5])
							}

							//On enleve de la liste de joueur celui qui est éliminé
							contain, i := Contains(playerInGame, processedRequest[3])
							if contain {
								playerInGame[i] = playerInGame[len(playerInGame)-1]
								playerInGame[len(playerInGame)-1] = ""
								playerInGame = playerInGame[:len(playerInGame)-1]
							}

							w <- strings.Join(processedRequest, ":::")
						} else if processedRequest[2] == "VILLWIN" || processedRequest[2] == "WOLFWIN" {
							if processedRequest[2] == "VILLWIN" {
								fmt.Println("Félicitation aux villageois, ils ont gagnés")
							} else if processedRequest[2] == "WOLFWIN" {
								fmt.Println("Félicitation aux loups-garous, ils ont gagnés !")
							}
							gs.State = "ENDOFGAME"
							w <- strings.Join(processedRequest, ":::")
						} else {
							w <- strings.Join(processedRequest, ":::")
						}
					}
				} else if processedRequest[1] == "CHAT" { // Gestion du Chat
					rep, i := Contains(pendingRequest, processedRequest[0])

					if rep { //Si on est à l'origine de la commande reçu

						pendingRequest[i] = pendingRequest[len(pendingRequest)-1]
						pendingRequest[len(pendingRequest)-1] = ""
						pendingRequest = pendingRequest[:len(pendingRequest)-1]

					} else { //Si on n'est pas à l'origine de la commande reçu (on n'est pas le leader)
						if processedRequest[2] == "DEAD" && gs.Dead {
							fmt.Println("[MORT] " + processedRequest[3] + " : " + processedRequest[4])
							fmt.Print(">> ")
						} else if processedRequest[2] == "ALIVE" && !gs.Dead{
							fmt.Println(processedRequest[3] + " : " + processedRequest[4])
							fmt.Print(">> ")
						} else if processedRequest[2] == "WOLF" && gs.State == "VOTEWOLF" && pair.Role == 1 {
							fmt.Println("[WOLF] " + processedRequest[3] + " : " + processedRequest[4])
							fmt.Print(">> ")
						}
						w <- strings.Join(processedRequest, ":::")
					}

				}
			}
		}
	}
}

/**
 * Ecrit au successeur
 */
func NetworkListenerForWrite(w chan string) {
	for {
		command := <-w
		pair.Successeur.Write([]byte(command + "\n"))

		if gs.State == "ENDOFGAME" || pair.Successeur == nil{			
			os.Exit(0)
		}
	}

}

/**
 * Gere les nouvelles connexion au réseau
 */
func NetworkListenerForNewConnection(conn net.Conn) {

	request, _ := bufio.NewReader(conn).ReadString('\n')

	processedRequest := ProcessRequest(request)
	if processedRequest[1] == "COMMAND" {

		if processedRequest[2] == "ENTER" {
			// Il veux entrer dans le réseau
			if pair.Successeur != nil {

				ip := strings.Split(pair.Successeur.RemoteAddr().String(), ":")[0]
				conn.Write([]byte(modele.RandString(10) + ":::COMMAND:::IPSUC:::" + ip + "\n"))

			} else {

				ip := strings.Split(conn.LocalAddr().String(), ":")[0]
				conn.Write([]byte(modele.RandString(10) + ":::COMMAND:::IPSUC:::" + ip + "\n"))

			}
			pair.Successeur = conn
		}

		if processedRequest[2] == "CONNECT" {
			if pair.Predecesseur != nil {
				pair.Predecesseur.Close()
			}
			pair.Predecesseur = conn
		}
	}
}

func getMacAddr() (addr string) { //on utilise pas le paramètre ????
	interfaces, err := net.Interfaces()
	if err == nil {
		for _, i := range interfaces {
			if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {
				// Don't use random as we have a real address
				return i.HardwareAddr.String()
			}
		}
	}
	return
}

/**
 * Gestion des commandes
 */
func communication(w chan string) {
	var (
		scanner = bufio.NewScanner(os.Stdin)
	)

	fmt.Println("************************************************")
	fmt.Println("********************  HELP  ********************")
	fmt.Println("************************************************")
	fmt.Println("/chat <votreMessage>     //Cela vous permet d'envoyer un message, il sera traité differement en fonction de votre rôle et du moment de la journée dans le jeu")
	fmt.Println("/vote <pseudo>           //Permet de voter lors des différents votes")
	fmt.Println("/ready              	  //Permet de lancer la partie pour le créateur")
	fmt.Println("/chat <votreMessage>     //Permet d'afficher l'aide")
	fmt.Println("**********************************************")

	if pair.Leader {
		fmt.Println("Ecrivez \"ready\" quand vous voulez lancer la partie")
		fmt.Print(">> ")
	}

	text := ""

	for {

		scanner.Scan()
		text = scanner.Text()
		textSplit := strings.Split(text, " ")

		if textSplit[0] == "/chat" { //Si le texte rentrée n'est pas "ready"
			textSplit = append(textSplit[:0], textSplit[(1):]...)
			msg := strings.Join(textSplit, " ")

			if gs.Dead {
				fmt.Println("[MORT] " + pair.Pseudo + " : " + msg)
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::CHAT:::DEAD:::" + pair.Pseudo + ":::" + msg
			} else if !gs.Dead && pair.Role == 1 && gs.State == "VOTEWOLF" {
				fmt.Println("[WOLF] " + pair.Pseudo + " : " + msg)
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::CHAT:::WOLF:::" + pair.Pseudo + ":::" + msg
			} else {
				fmt.Println(pair.Pseudo + " : " + msg)
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::CHAT:::ALIVE:::" + pair.Pseudo + ":::" + msg
			}

		} else if textSplit[0] == "/ready" && pair.Leader == true && gs.State == "WAITSTART" && !gs.Dead {

			gs.State = "STARTING"
			fmt.Println("Lancement de la partie")
			id := modele.RandString(10)
			pendingRequest = append(pendingRequest, id)
			w <- id + ":::COMMAND:::ELECTION:::" + strconv.FormatInt(pair.Ip, 10)

		} else if textSplit[0] == "/vote" && gs.State == "VOTEWOLF" && pair.Role == 1 && !gs.Dead {

			truPlayer, _ := Contains(playerInGame, textSplit[1])
			if truPlayer {
				fmt.Println("Vous avez voté pour : " + textSplit[1])
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::COMMAND:::LGVOTEOK:::" + textSplit[1] + ":::" + pair.Pseudo
				nbVote++
			} else {
				fmt.Println("Ce " + textSplit[1] + " n'existe pas, voici la liste des joueurs")
				fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
			}

		} else if textSplit[0] == "/vote" && gs.State == "VOTEVILL" && !gs.Dead {

			truPlayer, _ := Contains(playerInGame, textSplit[1])
			if truPlayer {
				fmt.Println("Vous avez voté pour : " + textSplit[1])
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::COMMAND:::VILLVOTEOK:::" + textSplit[1] + ":::" + pair.Pseudo
				nbVote++
			} else {
				fmt.Println("Le joueur :  " + textSplit[1] + " n'existe pas, voici la liste des joueurs")
				fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
			}

		} else if textSplit[0] == "/vote" && gs.State == "HUNTERVOTE" && pair.Role == 2 && gs.Dead {

			truPlayer, _ := Contains(playerInGame, textSplit[1])
			if truPlayer {
				fmt.Println("Vous avez chassé : " + textSplit[1])
				id := modele.RandString(10)
				pendingRequest = append(pendingRequest, id)
				w <- id + ":::COMMAND:::HUNTERVOTEOK:::" + textSplit[1] + ":::" + pair.Pseudo
			} else {
				fmt.Println("Le joueur :  " + textSplit[1] + " n'existe pas, voici la liste des joueurs")
				fmt.Println("[" + strings.Join(playerInGame, "],[") + "]")
			}

		} else if textSplit[0] == "/help" {
			fmt.Println("************************************************")
			fmt.Println("********************  HELP  ********************")
			fmt.Println("************************************************")
			fmt.Println("/chat <votreMessage>     //Cela vous permet d'envoyer un message, il sera traité differement en fonction de votre rôle et du moment de la journée dans le jeu")
			fmt.Println("/vote <pseudo>           //Permet de voter lors des différents votes")
			fmt.Println("/ready              	  //Permet de lancer la partie pour le créateur")
			fmt.Println("/chat <votreMessage>     //Permet d'afficher l'aide")
			fmt.Println("**********************************************")
		} else {
			fmt.Println("Votre commande n'est pas reconnu : " + text)
		}

		if gs.State == "ENDOFGAME" {
			os.Exit(0)
		}
	}
}