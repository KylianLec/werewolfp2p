package controlleurs

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func MenuManager() {

	var (
		bonneEntree = false // variable qui permet de vérifier si l'utilisateur a rentré la valeur qu'on attend de lui
		textInt     = 0
		err         error
		scanner     = bufio.NewScanner(os.Stdin)
	)

	for bonneEntree == false {
		fmt.Println("Bienvenue sur le loups Garou")
		fmt.Println("Que voulez vous faire ?")
		fmt.Println("1. Creer un serveur")
		fmt.Println("2. Rejoindre un serveur")
		fmt.Println("3. Quitter")
		fmt.Print(">> ")
		scanner.Scan()
		textInt, err = strconv.Atoi(scanner.Text())
		if err != nil { //vérifier si l'utilisateur a rentré un nombre
			fmt.Println("Ce n'est pas un chiffre, recommencez !")
		} else if textInt != 1 && textInt != 2 && textInt != 3 {
			fmt.Println("Chiffre incorrect, recommencez !")
		} else {
			bonneEntree = true
		}
	}

	if textInt == 1 {
		// on veux cree un serveur
		CreateServer(true)

	} else if textInt == 2 {
		// on veux rejoindre un serveur
		JoinServer()

	} else if textInt == 3 {
		// on veux rejoindre un serveur
		os.Exit(0)

	}
}
