package controlleurs

import (
	"Werewolf/modele"
	"fmt"
	"bufio"
	"os"
)
/* TODO
func SendRequest(pendingRequest []int, w chan string, request string) {

}*/

func ActionHunter(playerInGame []string, w chan string) {
	gs.State = "HUNTERVOTE"
	scanner := bufio.NewScanner(os.Stdin)
	huntedPlayer := ""
			
	fmt.Println("Qui voulez vous chasser ?")
	fmt.Println(playerInGame)
	huntedPlayer = scanner.Text()
	pseudoCorrect, _ := Contains(playerInGame, huntedPlayer)

	for !pseudoCorrect {
		fmt.Println("Ce joueur n'existe :o")
		fmt.Println("Qui voulez vous chasser ?")
		fmt.Println(playerInGame)
		scanner.Scan()
		huntedPlayer = scanner.Text()
		pseudoCorrect, _ = Contains(playerInGame, huntedPlayer)
	}

	id := modele.RandString(10)
	pendingRequest = append(pendingRequest, id)
	w <- id + ":::COMMAND:::KILL:::" + huntedPlayer + ":::HUNTED"
}