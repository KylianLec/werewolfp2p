package controlleurs

import (
	"Werewolf/modele"
	"fmt"
	"math/rand"
	"strings"
)

func PseudoCorrect(pseudo string) bool {
	var correct bool

	pseudoSplited := strings.Split(pseudo, "")

	for i := range pseudoSplited {
		if pseudoSplited[i] == " " {
			correct = false
			break
		} else if pseudoSplited[i] == ":" && i <= len(pseudoSplited)-3 {
			if pseudoSplited[i+1] == ":" && pseudoSplited[i+2] == ":" {
				correct = false
				break
			}
		} else {
			correct = true
		}
	}

	return correct
}

func Contains(arr []string, str string) (bool, int) {
	for index, a := range arr {
		if a == str {
			return true, index
		}
	}
	return false, -1
}

func ContainsMap(arr map[string]int, str string) bool {
	for key := range arr {
		if key == str {
			return true
		}
	}
	return false
}

func getVotedPlayer(votes []string) string {
	var voteWithName map[string]int
	voteWithName = make(map[string]int)

	for i := 0; i < len(votes); i++ {
		if ContainsMap(voteWithName, votes[i]) {
			voteWithName[votes[i]]++
		} else {
			voteWithName[votes[i]] = 0
		}
	}

	var votedPlayer []string
	nbVote := 0

	for key, value := range voteWithName {
		if value > nbVote {
			votedPlayer = votedPlayer[len(votedPlayer):]
			votedPlayer = append(votedPlayer, key)
			nbVote = value
		} else if value == nbVote {
			votedPlayer = append(votedPlayer, key)
		}
	}

	var player string

	if len(votedPlayer) > 1 {
		player = votedPlayer[rand.Intn(len(votedPlayer))]
	} else if len(votedPlayer) == 1 {
		player = votedPlayer[0]
	} else {
		fmt.Println("Erreur function getVotedPlayer")
	}

	return player
}

func DisplayNetworkState(pair modele.Pair) {
	if pair.Successeur != nil {
		fmt.Println("Connexion sur le successeur : " + pair.Successeur.LocalAddr().String() + "<-->" + pair.Successeur.RemoteAddr().String())
	} else {
		fmt.Println("Pas de connexion sur le successeur")
	}
	if pair.Predecesseur != nil {
		fmt.Println("Connexion sur le predecesseur : " + pair.Predecesseur.LocalAddr().String() + "<-->" + pair.Predecesseur.RemoteAddr().String())
	} else {
		fmt.Println("Pas de connexion sur le predecesseur")
	}
}

func ProcessRequest(command string) []string {
	process := strings.Split(strings.TrimSuffix(command, "\n"), ":::")
	return process
}

func ConcatIp(pair modele.Pair) string {
	var concatIp string = pair.Successeur.LocalAddr().String()

	concatIp = strings.Join(strings.Split(concatIp, "."), "") //on enleve les '.'
	concatIp = strings.Join(strings.Split(concatIp, ":"), "") //on enleve les ':'

	return concatIp
}
